title:      "Project 04: Just For Fun"
icon:       fa-code
navigation: []
internal:
external:
body:       |

  ## Overview

  The last book of the semester, [Just For Fun], examines the life of [Linus
  Torvalds] and the deveopment of the [Linux] operating system.

  The goal of the final project is for you to mimic [Linus Torvalds] and work
  on an [open source] project "just for fun".

  ## Requirements

  Working in groups of **2** - **4**, students must work on an [open source]
  project that meets **one** of the following formats:

  1. The group makes a **reasonable** contribution to an **existing** [open
  source] project.  This can be a mainstream project or an obscure one, but the
  group must follow the community standards (ie. pull requests, mailing list
  patches, etc.).  The group must document:

      - The contribution made by the group.

      - Evidence of communication between the group and the maintainers.

      - The status of their contribution.

  2. Alternatively, the group can **create** their own [open source] project.
  This can be a new project or based on previous work.  This custom [open
  source] project must include the following:

      - A `LICENSE` file which contains the [open source] license for the
        project.

      - A `HACKING` file which contains instructions for how to contribute to
        the project (workflow, coding style, etc.)

      - A web page that describes and advertises their project.

  The source code for your project should be stored on an online repository
  such as [GitHub], [GitLab], or [Bitbucket].

  ## Timeline

  On **Tuesday, May 2**, the class period will be used as a **hackathon** where
  groups can work on finishing up their project.

  <div class="alert alert-success" markdown="1">
  #### <i class="fa fa-cutlery"></i> Hacking Fuel

  To help fuel the creative enterprise, chinese food and coke will be served,
  provided the necessary funding can be allocated.

  If you are interesed in helping pool resources, please let the instructor
  know.

  </div>

  On **Monday, May 8** at **10:30AM**, students will present their projects.

  - The presentation should:

      - For option 1: Describe the project the group worked on, explain its
        [open source] background, discuss the contribution made by the group,
        describe the communication between the group and the maintainers, and
        provide an update on the status of the contribution.

          *Links to provide evidence of this information should be included in
          the presentation.*

      - For option 2: Describe the project the group worked on, explain the
        [open source] license chosen, discuss the project workflow and coding
        style, present the web page for the project, and provide an update on
        any changes made to the project (if it is an existing one) or
        demonstrate the new project.

          *Links to these artifacts should be included in the presentation.*

  - Each group will have **5** - **10** minutes to present.

  ### Submission

  Once you have organized your group and have code repository, please fill out
        the following [form](https://goo.gl/forms/DcTpFVELZNvcS0o22) to let us
        know where to find it:

  <iframe src="https://docs.google.com/a/nd.edu/forms/d/e/1FAIpQLSdgN7brpa7XPnZb-EJ5PnILEsMT_jvJ9_mh8SB4ZRgYAX3TsA/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>

  Please include a link to your **presentation**.  Inside the presentation,
  please include links to your project artifacts (source code repository,
  project web page, evidence of communication, etc.).

  [Just For Fun]:   https://archive.org/details/JustForFun
  [GitHub]:         https://github.com
  [GitLab]:         https://gitlab.com
  [BitBucket]:      https://bitbucket.org
  [open source]:    https://en.wikipedia.org/wiki/Open-source_model
  [Linus Torvalds]: https://en.wikipedia.org/wiki/Linus_Torvalds
  [Linux]:          https://www.kernel.org/
