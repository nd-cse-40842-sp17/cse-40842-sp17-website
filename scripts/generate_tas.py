#!/usr/bin/env python2

import csv
import random
import yaml

graders = [
    { 'netid': 'aliu1'   , 'blogs': [] },
    { 'netid': 'sjohns25', 'blogs': [] },
    { 'netid': 'dmattia' , 'blogs': [] },
]

blogs = []

for blog in csv.DictReader(open('data/form.csv')):
    blogs.append({
        'netid':    blog['NetID'],
        'url':      blog['URL'],
    })

random.shuffle(blogs)

grader = 0
while blogs:
    blog = blogs.pop()
    graders[grader]['blogs'].append(blog)
    grader = (grader + 1) % len(graders)

print yaml.dump(graders, default_flow_style=False)
